/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef ALLOC_NEW_PERSISTENT_H
#define	ALLOC_NEW_PERSISTENT_H

#ifdef __GNUC__
#  define likely(x)   __builtin_expect(!!(x), 1)
#  define unlikely(x) __builtin_expect(!!(x), 0)
#else
#  define likely(x)   !!(x)
#  define unlikely(x) !!(x)
#endif

#if defined(USE_MEMKIND) && defined(RECORDMANAGER_PMEM_SUPPORT)

#define PMEM_MOUNT_PATH "/mnt/pmem1_mount/"

#include "plaf.h"
#include "pool_interface.h"
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <memkind.h>

//__thread long long currentAllocatedBytes = 0;
//__thread long long maxAllocatedBytes = 0;

template<typename T = void>
class allocator_new_persistent : public allocator_interface<T> {
    PAD; // post padding for allocator_interface
    struct memkind* kind;
    PAD;
public:
    template<typename _Tp1>
    struct rebind {
        typedef allocator_new_persistent<_Tp1> other;
    };
    
    void setupKind(){
        int err = memkind_create_pmem(PMEM_MOUNT_PATH, 0, &kind);
        if (err) {
            printf("Error creating pmem kind\n");
            exit(-1);            
        }
        printf("-----------------------------------\n");
        printf("PMEM KIND INITIALIZED SUCCESSFULLY\n");
        printf("Pmem mount at: %s\n", PMEM_MOUNT_PATH);
        printf("-----------------------------------\n");
    }

    void setKind(memkind* mem_kind){
        if (!kind) {
            kind = mem_kind;
        }        
    }

    // reserve space for ONE object of type T
    T* allocate(const int tid) {
        if(unlikely(!kind)) {
            setupKind();
        }
        // allocate a new object
        MEMORY_STATS {
            this->debug->addAllocated(tid, 1);
            VERBOSE {
                if ((this->debug->getAllocated(tid) % 2000) == 0) {
                    debugPrintStatus(tid);
                }
            }
//            currentAllocatedBytes += sizeof(T);
//            if (currentAllocatedBytes > maxAllocatedBytes) {
//                maxAllocatedBytes = currentAllocatedBytes;
//            }
        }
        // return new T; //(T*) malloc(sizeof(T));
        return (T*) memkind_malloc(kind, sizeof(T));
    }
    // T* allocate_kind(const int tid, memkind* kind) {
    //     MEMORY_STATS {
    //         this->debug->addAllocated(tid, 1);
    //         VERBOSE {
    //             if ((this->debug->getAllocated(tid) % 2000) == 0) {
    //                 debugPrintStatus(tid);
    //             }
    //         }
    //     }
    //     return (T*) memkind_malloc(kind, sizeof(T));
    // }

    void deallocate(const int tid, T * const p) {
        // note: allocators perform the actual freeing/deleting, since
        // only they know how memory was allocated.
        // pools simply call deallocate() to request that it is freed.
        // allocators do not invoke pool functions.
        MEMORY_STATS {
            this->debug->addDeallocated(tid, 1);
//            currentAllocatedBytes -= sizeof(T);
        }
#if !defined NO_FREE
        //delete p;
        memkind_free(kind, p);
#endif
    }
//     void deallocate_kind(const int tid, T * const p, memkind* kind) {       
//         MEMORY_STATS {
//             this->debug->addDeallocated(tid, 1);          
//         }
// #if !defined NO_FREE
//          memkind_free(kind, p);
// #endif
//     }
    void deallocateAndClear(const int tid, blockbag<T> * const bag) {
#ifdef NO_FREE
        bag->clearWithoutFreeingElements();
#else
        while (!bag->isEmpty()) {
            T* ptr = bag->remove();
            deallocate(tid, ptr);
        }
#endif
    }
//     void deallocateAndClear_kind(const int tid, blockbag<T> * const bag, memkind* kind) {
// #ifdef NO_FREE
//         bag->clearWithoutFreeingElements();
// #else
//         while (!bag->isEmpty()) {
//             T* ptr = bag->remove();
//             deallocate(tid, ptr, kind);
//         }
// #endif
//     }
    
    void debugPrintStatus(const int tid) {
//        std::cout<</*"thread "<<tid<<" "<<*/"allocated "<<this->debug->getAllocated(tid)<<" objects of size "<<(sizeof(T));
//        std::cout<<" ";
////        this->pool->debugPrintStatus(tid);
//        std::cout<<std::endl;
    }
    
    void initThread(const int tid) {}
    void deinitThread(const int tid) {}
    
    allocator_new_persistent(const int numProcesses, debugInfo * const _debug)
            : allocator_interface<T>(numProcesses, _debug) {
        VERBOSE DEBUG std::cout<<"constructor allocator_new_persistent"<<std::endl;
    }
    ~allocator_new_persistent() {
        VERBOSE DEBUG std::cout<<"destructor allocator_new_persistent"<<std::endl;
    }
};

#elif defined(USE_LIBVMMALLOC) && defined(RECORDMANAGER_PMEM_SUPPORT)

#define LIBMMALLOC_SO_FILE_PATH "/usr/local/lib/libvmmalloc.so"

#include "plaf.h"
#include "pool_interface.h"
#include <cstdlib>
#include <cassert>
#include <iostream>
#include "dlfcn.h"

//__thread long long currentAllocatedBytes = 0;
//__thread long long maxAllocatedBytes = 0;

template<typename T = void>
class allocator_new_persistent : public allocator_interface<T> {
    PAD; // post padding for allocator_interface
    void* (*pmallocFunc)(size_t);
	void (*pfreeFunc)(void*);  
    PAD;
    
public:
    template<typename _Tp1>
    struct rebind {
        typedef allocator_new_persistent<_Tp1> other;
    };
    
    void initLibvmmalloc() {
        void *libvmmalloc = dlopen(LIBMMALLOC_SO_FILE_PATH, RTLD_NOW | RTLD_LOCAL);        
        
        if (!libvmmalloc) {
            setbench_error("Error opening libvammloc.so with dlopen.\nExiting\n");
            exit(-1);
        }
        
        *(void **)(&pmallocFunc) = dlsym(libvmmalloc, "malloc");                
        *(void **)(&pfreeFunc) = dlsym(libvmmalloc, "free");		 
        printf("Initialized Allocator New with Libvmmalloc\n");   
    }

    // reserve space for ONE object of type T
    T* allocate(const int tid) {
        if(unlikely(!pmallocFunc)) {
            initLibvmmalloc();
        }

        // allocate a new object
        MEMORY_STATS {
            this->debug->addAllocated(tid, 1);
            VERBOSE {
                if ((this->debug->getAllocated(tid) % 2000) == 0) {
                    debugPrintStatus(tid);
                }
            }
//            currentAllocatedBytes += sizeof(T);
//            if (currentAllocatedBytes > maxAllocatedBytes) {
//                maxAllocatedBytes = currentAllocatedBytes;
//            }
        }
        // return (T*) malloc(sizeof(T));
        return (T*) pmallocFunc(sizeof(T));
    }
    void deallocate(const int tid, T * const p) {
        // note: allocators perform the actual freeing/deleting, since
        // only they know how memory was allocated.
        // pools simply call deallocate() to request that it is freed.
        // allocators do not invoke pool functions.
        MEMORY_STATS {
            this->debug->addDeallocated(tid, 1);
//            currentAllocatedBytes -= sizeof(T);
        }
#if !defined NO_FREE        
        // free(p);
        pfreeFunc(p);
#endif
    }

//     T* pallocate(const int tid) {
//         if(unlikely(!pmallocFunc)) {
//             initLibvmmalloc();
//         }

//         // allocate a new object
//         MEMORY_STATS {
//             this->debug->addAllocated(tid, 1);
//             VERBOSE {
//                 if ((this->debug->getAllocated(tid) % 2000) == 0) {
//                     debugPrintStatus(tid);
//                 }
//             }
//         }
//         return (T*) pmallocFunc(sizeof(T));
//     }
//     void pdeallocate(const int tid, T * const p) {
//         MEMORY_STATS {
//             this->debug->addDeallocated(tid, 1);
//         }
// #if !defined NO_FREE        
//         pfreeFunc(p);
// #endif
//     }



//     void deallocateAndClear(const int tid, blockbag<T> * const bag, bool isPmem = false) {
// #ifdef NO_FREE
//         bag->clearWithoutFreeingElements();
// #else
//         while (!bag->isEmpty()) {
//             T* ptr = bag->remove();
//             if (isPmem) {
//                 pdeallocate(tid, ptr);
//             }
//             else {
//                 deallocate(tid, ptr);
//             }
//         }
// #endif
//     }


        void deallocateAndClear(const int tid, blockbag<T> * const bag) {
#ifdef NO_FREE
        bag->clearWithoutFreeingElements();
#else
        while (!bag->isEmpty()) {
            T* ptr = bag->remove();
            deallocate(tid, ptr);
        }
#endif
    }
    
    
    void debugPrintStatus(const int tid) {
//        std::cout<</*"thread "<<tid<<" "<<*/"allocated "<<this->debug->getAllocated(tid)<<" objects of size "<<(sizeof(T));
//        std::cout<<" ";
////        this->pool->debugPrintStatus(tid);
//        std::cout<<std::endl;
    }
    
    void initThread(const int tid) {}
    void deinitThread(const int tid) {}
    
    allocator_new_persistent(const int numProcesses, debugInfo * const _debug)
            : allocator_interface<T>(numProcesses, _debug) {
        VERBOSE DEBUG std::cout<<"constructor allocator_new_persistent"<<std::endl;
    }
    ~allocator_new_persistent() {
        VERBOSE DEBUG std::cout<<"destructor allocator_new_persistent"<<std::endl;
    }
};

#else 
#error "Persistent alloctor used without proper pmem alloctor defines" 
#endif

#endif	/* ALLOC_NEW_H */
